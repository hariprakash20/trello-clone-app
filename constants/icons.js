import heart from "../assets/icons/heart.png";
import menu from "../assets/icons/menu.png";
import search from "../assets/icons/search.png";
import filter from "../assets/icons/filter.png";
import left from "../assets/icons/left.png";
import heartOutline from "../assets/icons/heart-ol.png";
import share from "../assets/icons/share.png";
import location from "../assets/icons/location.png";
import chevronLeft from '../assets/icons/chevron-left.png'
import chevronRight from '../assets/icons/chevron-right.png'
import menu_icon from '../assets/icons/menu_icon.png'
import plus from '../assets/icons/plus.png';
import search_icon from '../assets/icons/search_icon.png'
import bell_icon from '../assets/icons/bell_icon.png'
import more_options from '../assets/icons/more_options.png'
import board_icon from '../assets/icons/board_icon.png'
import card_icon from '../assets/icons/card_icon.png'
import template_icon from '../assets/icons/template_icon.png'

export default {
  heart,
  menu,
  search,
  filter,
  left,
  heartOutline,
  share,
  location,
  chevronLeft,
  chevronRight,
  menu_icon,
  plus,
  search_icon,
  bell_icon,
  more_options,
  board_icon,
  card_icon,
  template_icon
};
