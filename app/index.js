import {
  SafeAreaView,
  ScrollView,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  StyleSheet,
} from "react-native";
import { Stack, useRouter } from "expo-router";
import ScreenHeaderBtn from "../components/common/header/ScreenHeaderBtn";
import { COLORS, icons, images } from "../constants";
import FloatingAddButton from "../components/Home/FloatingAddButton/FloatingAddButton";
import { useEffect, useState } from "react";
import axios from "axios";
import { API_KEY, TOKEN } from "@env";

export default function Page() {
  const [addBtnExpanded, setAddBtnExpanded] = useState(false);
  const [boards, setBoards] = useState([]);
  const router = useRouter();

  useEffect(() => {
    axios
      .get(
        `https://api.trello.com/1/members/me/boards?key=${API_KEY}&token=${TOKEN}`
      )
      .then((response) => setBoards(response.data))
      .catch((error) => console.log("Error occured in fetching API" + error));
  }, []);

  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: COLORS.lightWhite,
        color: COLORS.lightWhite,
      }}
    >
      <Stack.Screen
        options={{
          headerStyle: {
            backgroundColor: COLORS.lightWhite,
            color: COLORS.lightWhite,
          },
          headerLeft: () => (
            <ScreenHeaderBtn iconUrl={icons.menu_icon} dimension="60%" />
          ),
          headerRight: () => (
            <View
              style={{ flexDirection: "row", justifyContent: "space-between" }}
            >
              <ScreenHeaderBtn
                iconUrl={icons.search_icon}
                style={{}}
                dimension="100%"
              />
              <ScreenHeaderBtn iconUrl={icons.bell_icon} dimension="100%" />
            </View>
          ),
          headerTitle: "Boards",
        }}
      />
      <View
        style={{
          flexDirection: "row",
          backgroundColor: "#dee0df",
          height: 40,
          alignItems: "center",
          justifyContent: "space-between",
          margin: 2,
        }}
      >
        <Text style={{ right: -10 }}>hari prakash's Workspace</Text>
        <Image
          style={{ height: 10, width: 15, right: 15 }}
          source={icons.more_options}
        ></Image>
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        {boards.length > 0 ? (
          boards.map((each) => {
            return (
              <TouchableOpacity style={styles.eachBoard} key={each.id} onPress={() => router.push(`/each-board/${each.id}`)}>
                <View style={styles.boardContents}>
                  <View style={styles.boardImage} />
                  <Text>{each.name}</Text>
                </View>
              </TouchableOpacity>
            );
          })
        ) : (
          <View>
            <Text>No Boards to display</Text>
          </View>
        )}
      </ScrollView>

      <FloatingAddButton
        onClick={() => setAddBtnExpanded((prevState) => !prevState)}
        addBtnExpanded={addBtnExpanded}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  eachBoard: {
    flexDirection: "row",
    height: 70,
    alignItems: "center",
    backgroundColor: "#f7f9fa",
    margin: 10,
  },
  boardImage: {
    height: 50,
    width: 50,
    backgroundColor: "#42b3f5",
    borderRadius: 10,
  },
  boardContents: {
    flexDirection: "row",
    margin: 15,
    alignItems: "center",
  },
});