import { TextInput, Text, View, StyleSheet, Button } from "react-native";
import { Picker } from "@react-native-picker/picker";
import { useState } from "react";
import { API_KEY, TOKEN } from "@env";
import { useRouter } from "expo-router";
import axios from "axios";

const NewBoard = () => {
  const [boardName, setBoardName] = useState("");
  const [workspace, setworkspace] = useState("hari prakash's Workspace");
  const [visibility, setVisibility] = useState("Public");
  const router = useRouter();
  let validated = workspace && visibility && boardName !== "";

  function createNewBoard() {
    axios
      .post(
        `https://api.trello.com/1/boards/?name=${boardName}&key=${API_KEY}&token=${TOKEN}`
      )
      .then((response) => response.data)
      .catch((error) =>
        console.log("Error occured in creating board" + boardName + error)
      );
      router.back();
  }
  return (
    <View style={styles.inputContainer}>
      <TextInput
        placeholder="Board Name"
        style={styles.inputFields}
        onChangeText={setBoardName}
      />
      <Picker style={styles.inputFields} placeholder="Workspace">
        <Picker.Item
          label="hari prakash's Workspace"
          value="hari prakash's Workspace"
        />
      </Picker>
      <Picker style={styles.inputFields} placeholder="Visibility">
        <Picker.Item label="Private" value="Private" />
        <Picker.Item label="Workspace" value="Workspace" />
        <Picker.Item label="Public" value="Public" />
      </Picker>
      <Button
        style={styles.createBtn}
        title="Create board"
        disabled={validated ? false : true}
        onPress={() => createNewBoard()}
      />
    </View>
  );
};

export default NewBoard;

export const styles = StyleSheet.create({
  inputFields: {
    margin: 10,
    borderBottomWidth: 2,
    borderColor: "green",
  },
  inputContainer: {
    flex: 1,
    margin: 10,
  },
  createBtn: {
    alignSelf: "flex-end",
  },
});
