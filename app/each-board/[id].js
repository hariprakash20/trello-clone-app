import { Text, SafeAreaView, StyleSheet, View } from "react-native";
import { Stack, useSearchParams } from "expo-router";
import { useEffect, useState } from "react";
import { API_KEY, TOKEN } from "@env";
import axios from "axios";
import { Card } from "@rneui/themed";

const EachBoard = () => {
  const params = useSearchParams();
  const id = params.id;
  const [list, setList] = useState([]);
  const [boardName, setBoardName] = useState("");

  useEffect(() => {
    axios
      .get(
        `https://api.trello.com/1/boards/${id}/lists?key=${API_KEY}&token=${TOKEN}`
      )
      .then((response) => setList(response.data))
      .catch((error) => console.log("Error occured in fetching Lists" + error));

    axios
      .get(
        `https://api.trello.com/1/boards/${id}?key=${API_KEY}&token=${TOKEN}`
      )
      .then((response) => setBoardName(response.data.name))
      .catch((error) =>
        console.log("Error occured in fetching Board data" + error)
      );
  }, []);
  // console.log(list);
  // console.log(boardName);
  return (
    <SafeAreaView>
      <Stack.Screen options={{ headerTitle: `${boardName}` }} />
      <View style={styles.listOflist}>
        
        {list.map((each) => {
          return (
            <Card key={each.id}>
              <Card.Title>{each.name}</Card.Title>
            </Card>
          );
        })}
      </View>
    </SafeAreaView>
  );
};

export default EachBoard;

const styles = StyleSheet.create({
  listOflist: {
    flexDirection: "row",
  },
});
