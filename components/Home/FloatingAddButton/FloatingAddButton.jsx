import { View, Image, TouchableOpacity, Text, StyleSheet } from "react-native";
import { COLORS, icons } from "../../../constants";
import { useRouter } from "expo-router";

const FloatingAddButton = ({ addBtnExpanded, onClick }) => {
    const router = useRouter();
  return (
    <View style={{ bottom: 30, right: 20, position: "absolute" }}>
      {addBtnExpanded && (
        <View style={styles.optionsContainer}>
          <TouchableOpacity style={styles.floatingAddButtonOptions}>
            <Text>Browse Templates</Text>
            <Image
              source={icons.template_icon}
              style={styles.optionIcon}
              resizeMode="cover"
            ></Image>
          </TouchableOpacity>
          <TouchableOpacity style={styles.floatingAddButtonOptions}>
            <Text>card</Text>
            <Image
              source={icons.card_icon}
              style={styles.optionIcon}
              resizeMode="cover"
            ></Image>
          </TouchableOpacity>
          <TouchableOpacity style={styles.floatingAddButtonOptions} onPress={() => router.push('/new-board/')} >
            <Text>Board</Text>
            <Image
              source={icons.board_icon}
              style={styles.optionIcon}
              resizeMode='contain'
            ></Image>
          </TouchableOpacity>
        </View>
      )}
      <View style={styles.optionsContainer}>
        <TouchableOpacity
          style={{
            backgroundColor: "green",
            width: 70,
            height: 70,
            borderRadius: 100,
            justifyContent: "flex-end",
            alignItems: "flex-end",
          }}
          onPress={onClick}
        >
          <Image
            source={icons.plus}
            style={{ width: "100%", height: "100%" }}
          ></Image>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default FloatingAddButton;

export const styles = StyleSheet.create({
  floatingAddButtonOptions: {
    flexDirection: "row",
    margin: 10,
  },
  optionsContainer: {
    alignItems: "flex-end",
  },
  optionIcon: {
    height: 50,
    width: 50,
    backgroundColor: "green",
    borderRadius: 100,
  },
});
